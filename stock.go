package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"
)

//struct to store ohlc info of each ticker in given timespan
type OHLC struct {
	O float64
	H float64
	L float64
	C float64
}

func writeCandlesToFile(interval int) {
	tradeStart, _ := time.Parse("2006-01-02 15:04:05.999999", "2019-01-30 07:00:00.000000") //might need
	file, _ := os.Open("trades/trades.csv")                                                 //to figure out how
	defer file.Close()                                                                      //to find it automatically
	reader := csv.NewReader(file)
	fileName := fmt.Sprintf("trades/candles_%vmin.csv", interval)
	output, _ := os.Create(fileName)
	defer output.Close()
	writer := csv.NewWriter(output)
	defer writer.Flush()
	tickerMap := make(map[string]*OHLC) //we need map to store ohlc ticker data of each time interval
	for curDate := tradeStart; ; {      //setting curDate variable to reflect the starting time of trades
		line, err := reader.Read()
		//we still need to print the last period to the file
		if err == io.EOF {
			for k, v := range tickerMap {
				result := []string{k, curDate.Format(time.RFC3339),
					strconv.FormatFloat(v.O, 'f', 2, 64),
					strconv.FormatFloat(v.H, 'f', 2, 64),
					strconv.FormatFloat(v.L, 'f', 2, 64),
					strconv.FormatFloat(v.C, 'f', 2, 64),
				}
				writer.Write(result)
				delete(tickerMap, k)
			}
			break
		}
		ticker := line[0]
		price, _ := strconv.ParseFloat(line[1], 64)
		dateTime, _ := time.Parse("2006-01-02 15:04:05.999999", line[3])
		//setting up the interval ending time
		nextDate := curDate.Add(time.Duration(interval) * time.Minute)
		//skipping all the unnecessary transactions
		if dateTime.Hour() >= 0 && dateTime.Hour() < 7 {
			continue
		}
		//if dateTime of transaction exceeds the interval ending time, we write the ohlc result to the file,
		//delete all the contents of our map and set curDate to reflect the relevant time interval (some intervals might
		// be skipped)
		if nextDate.Sub(dateTime) <= 0 {
			for k, v := range tickerMap {
				result := []string{k, curDate.Format(time.RFC3339),
					strconv.FormatFloat(v.O, 'f', 2, 64),
					strconv.FormatFloat(v.H, 'f', 2, 64),
					strconv.FormatFloat(v.L, 'f', 2, 64),
					strconv.FormatFloat(v.C, 'f', 2, 64),
				}
				writer.Write(result)
				delete(tickerMap, k)
			}
			//this is the part where we find out the relevant date
			curDate = nextDate.Add(time.Minute * time.Duration(interval) * time.Duration(int(dateTime.Sub(nextDate).Minutes())/interval))
		}
		//checking whether the ticker is present in the map
		ohlc, ok := tickerMap[ticker]
		if !ok {
			//if it isn't we create it and assign a new ohlc struct to it
			tickerMap[ticker] = &OHLC{price, price, price, price}
			continue
		}
		//if it is we update ohlc if necessary
		if price > ohlc.H {
			ohlc.H = price
		} else if price < ohlc.L {
			ohlc.L = price
		}
		//we also update the closing price each time (we don't know if this is going to be the last transaction)
		ohlc.C = price
	}
}

func main() {
	writeCandlesToFile(5)
	writeCandlesToFile(30)
	writeCandlesToFile(240)
}
